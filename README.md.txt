Evaluacion:JAVA

La siguiente solucion expone una API RESTful de creacion de usuarios, con las siguientes
caracteristicas

- Los servicios Rest manejan JSON como notación
- La seguridad de los servicios es mediante JWT
- Base de datos temporal H2
- Documentacion con swagger

Al iniciar la aplicacion se ejecuta automaticamente el archivo data.sql
que contiene el script de creacion de tablas de USUARIO y TELEFONO,
tambien contiene el script de insert del usuario con email adm@adm.org y password admin 
, esto con el objetivo de porder crear el token con ese usuario

Levantada la aplicacion 
1) Se inicia en el puerto 8082
   
2) Para crear el token: POST http://localhost:8082/authenticate  
{
    "username":"admin@admin.org", 
     "password":"admin"
}
el token tiene expira en 
3) Obtener el listado de usuario GET http://localhost:8082/usuarios 
4) Crear un usuario POST http://localhost:8082/usuarios con el body
{
"name": "Juan Perez",
"email": "juan@perez.org",
"password": "pass12345",
"telefonos": [
{"number": "1234567",
"citycode": "1",
"countrycode": "591"
}
]
}
