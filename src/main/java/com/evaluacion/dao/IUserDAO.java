package com.evaluacion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.evaluacion.model.Usuario;

public interface IUserDAO extends JpaRepository<Usuario, Integer>{
	
	Usuario findByEmail(String emailAddress);
	
}
