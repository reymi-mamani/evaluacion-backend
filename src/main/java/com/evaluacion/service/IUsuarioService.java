package com.evaluacion.service;

import java.util.List;

import com.evaluacion.model.Usuario;

public interface IUsuarioService extends ICRUD<Usuario>{
	Usuario findByEmail(String emailAddress);
}
