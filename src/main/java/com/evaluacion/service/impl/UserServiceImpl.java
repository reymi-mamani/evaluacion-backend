package com.evaluacion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.evaluacion.dao.IUserDAO;
import com.evaluacion.dao.IUsuarioDao;
import com.evaluacion.model.Usuario;

@Service("userDetailService")
public class UserServiceImpl implements UserDetailsService{
	
	@Autowired
	private IUserDAO userDAO;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("loadUserByUsername "+username);		
		Usuario user = userDAO.findByEmail(username);
		if(user==null) {
			throw new UsernameNotFoundException(String.format("Usuario no existe ", username));
		}
		
		return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
		
	}

}
