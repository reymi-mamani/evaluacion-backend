package com.evaluacion.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.evaluacion.dao.IUsuarioDao;
import com.evaluacion.model.Telefono;
import com.evaluacion.model.Usuario;
import com.evaluacion.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService{

	@Autowired
	private IUsuarioDao dao;
	
	@Transactional
	public Usuario registrar(final Usuario usuario) {
		usuario.setCreated(LocalDateTime.now());
		usuario.setLast_login(LocalDateTime.now());
		usuario.setIsative(true);
		usuario.getTelefonos().forEach(new Consumer<Telefono>() {
			public void accept(Telefono d) {
				d.setUsuario(usuario);
			}
		});
		return dao.save(usuario);
	}

	public Usuario modificar(Usuario usuario) {
		usuario.setModified(LocalDateTime.now());
		return dao.save(usuario);
	}
	
	public Optional<Usuario> listarId(int id) {
		return dao.findById(id);
	}

	public List<Usuario> listar() {
		
		return dao.findAll();
	}

	public void eliminar(Usuario usuario) {
		usuario.setIsative(false);
		dao.delete(usuario);		
	}

	public void eliminarId(int id) {
		dao.deleteById(id);
	}

	@Override
	public Usuario findByEmail(String emailAddress) {
		return dao.findByEmail(emailAddress);
	}

	

}
