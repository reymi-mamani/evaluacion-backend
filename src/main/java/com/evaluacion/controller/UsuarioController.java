package com.evaluacion.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.evaluacion.exception.ModeloNotFoundException;
import com.evaluacion.model.Usuario;
import com.evaluacion.service.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private IUsuarioService service;
	

	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Usuario>> listar() {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		usuarios = service.listar();
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public Optional<Usuario> listarPorId(@PathVariable("id") Integer id) {
		Optional<Usuario> usr = service.listarId(id);
		if (!usr.isPresent()) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return service.listarId(id);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> registrar(@RequestBody Usuario usuario) {
		Usuario u = service.findByEmail(usuario.getEmail());
		if (u!=null) {
			throw new ModeloNotFoundException("El correo ya fue registrado ");
		} else {
			if (usuario.getEmail().matches("(\\S.*\\S)(@)(\\S.*\\S)(.\\S[a-z]{2,3})")) {
				Usuario pac = new Usuario();
				pac = service.registrar(usuario);
				
				return new ResponseEntity<Object>(pac,null,HttpStatus.CREATED);
			} else {
				throw new ModeloNotFoundException("Formato de correo incorrecto ");
			}
		}
	}

	@PutMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> modificar(@RequestBody Usuario usuario) {
		service.modificar(usuario);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		Optional<Usuario> usr = service.listarId(id);
		if (!usr.isPresent()) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO PARA ELIMINAR " + id);
		} else {
			service.eliminarId(id);
		}
	}

}
