package com.evaluacion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.evaluacion.model.Telefono;

import io.swagger.annotations.ApiModel;

@ApiModel(description="Telefonos del usuario")
@Entity
@Table(name = "telefono")
public class Telefono {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTelefono;
	
	@JsonIgnore 
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable=false)
	private Usuario usuario;
	
	@Column(name="number", nullable=false, length=20)
	private String number;
	
	@Column(name="citycode", nullable=false, length=10)
	private String citycode;
	
	@Column(name="countrycode", nullable=false, length=5)
	private String countrycode;

	public Integer getIdTelefono() {
		return idTelefono;
	}

	public void setIdTelefono(Integer idTelefono) {
		this.idTelefono = idTelefono;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}
	
	

}
