package com.evaluacion.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="Informacion de la tabla usuario")
@Entity
@Table(name = "usuario")
@NamedQuery(name = "Usuario.findByEmail",
query = "SELECT c FROM Usuario c WHERE c.email = ?1")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
	
	@ApiModelProperty(notes="Nombres deben tener minimo 3 caracteres")
	@Size(min=3, message="Nombres deben tener minimo 3 caracteres")
	@Column(name="name", nullable=false, length=70)
	private String name;
		
	@ApiModelProperty(notes="Email deben tener minimo 10 caracteres")
	@Size(min=10, message="Nombres deben tener minimo 10 caracteres")
	@Column(name="email", nullable=false, length=55, unique=true)    
	private String email;
	
	@Size(min=6, message="Password deben tener minimo 6 caracteres")
	@Column(name="password", nullable=false, length=15)
	private String password;
	
	@OneToMany(mappedBy="usuario", cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
			fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Telefono> telefonos;
	
	@JsonSerialize(using= ToStringSerializer.class)
	private LocalDateTime created;
	
	@JsonSerialize(using= ToStringSerializer.class)
	private LocalDateTime modified;
	
	@JsonSerialize(using= ToStringSerializer.class)
	private LocalDateTime last_login;
		
	private String token;
	
	private boolean isative;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Telefono> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	public LocalDateTime getLast_login() {
		return last_login;
	}

	public void setLast_login(LocalDateTime last_login) {
		this.last_login = last_login;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isIsative() {
		return isative;
	}

	public void setIsative(boolean isative) {
		this.isative = isative;
	}
	
	

}
